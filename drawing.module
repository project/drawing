<?php

/**
 * @file
 * The Drawing API module.
 */

/**
 * Implementation of hook_theme().
 */
function drawing_theme() {
  $drawing_themes = array();
  // all our themes have the same registry data:
  $drawing = array(
    'arguments' => array(
      'element' => NULL,
    ),
    'function' => 'theme_drawing',
  );
  // list of themes
  $themes = array(
    'drawing_canvas',
    'drawing_group',
    'drawing_file',
    'drawing_ellipse',
    'drawing_rectangle',
    'drawing_line',
    'drawing_polyline',
    'drawing_polygon',
    'drawing_path',
    'drawing_text',
    'drawing_circle',
    'drawing',
  );
  foreach ($themes as $theme) {
    $drawing_themes[$theme] = $drawing;
  }
  return $drawing_themes;
}

/**
 * Implementation of hook_menu().
 */
function drawing_menu() {
  $items = array();
  $items['admin/settings/drawing'] = array(
    'title' => 'Drawing API',
    'description' => 'Drawing API settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('drawing_admin'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Admin form for Drawing API settings.
 */
function drawing_admin() {
  $form = array();
  $form['drawing_type'] = array(
    '#type' => 'radios',
    '#title' => t('Set toolkit'),
    '#default_value' => variable_get('drawing_type', 'drawing_svg'),
    '#description' => t('Select which toolkit you want to enable on the site.'),
    '#options' => drawing_get_all_methods(),
  );
  return system_settings_form($form);
}

/**
 * Retrieves a canvas from a builder function, passes it on for
 * processing, and renders the canvas.
 *
 * @param $canvas_id
 *   The unique string identifying the desired canvas. If a function
 *   with that name exists, it is called to build the canvas array.
 *   Modules that need to generate the same canvas (or very similar canvases)
 *   using different $canvas_ids can implement hook_canvases(), which maps
 *   different $canvas_id values to the proper canvas building function.
 * @param ...
 *   Any additional arguments needed by the canvas building function.
 * @return
 *   The rendered canvas.
 */
function drawing_get_canvas($canvas_id) {
  $args = func_get_args();
  $canvas = call_user_func_array('drawing_retrieve_canvas', $args);
  drawing_prepare_canvas($args[0], $canvas);
  return drawing_render_canvas($canvas_id, $canvas);
}


/**
 * Retrieves the structured array that defines a given canvas.
 *
 * @param $canvas_id
 *   The unique string identifying the desired canvas. If a function
 *   with that name exists, it is called to build the canvas array.
 *   Modules that need to generate the same canvas (or very similar canvases)
 *   using different $canvas_ids can implement hook_canvases(), which maps
 *   different $canvas_id values to the proper canvas building function.
 * @param ...
 *   Any additional arguments needed by the canvas building function.
 */
function drawing_retrieve_canvas($canvas_id) {
  static $canvases;
  $args = func_get_args();
  $saved_args = $args;
  array_shift($args);

  if (!function_exists($canvas_id)) {
    // if you want to handle your canvas uniquely, do it with hook_canvases()
    if (!isset($canvases) || !isset($canvases[$canvas_id])) {
      $canvases = module_invoke_all('canvases', $saved_args);
    }
    $canvas_definition = $canvases[$canvas_id];
    if (isset($canvas_definition['callback arguments'])) {
      $args = array_merge($canvas_definition['callback arguments'], $args);
    }
    if (isset($canvas_definition['callback'])) {
      $callback = $canvas_definition['callback'];
    }
  }
  $canvas = call_user_func_array(isset($callback) ? $callback : $canvas_id, $args);
  $canvas['#parameters'] = $saved_args;
  return $canvas;
}

/**
 * Renders a structured canvas array into themed output.
 *
 * @param $canvas_id
 *   A unique string identifying the canvas for hook_drawing_canvas_alter
 *   functions.
 * @param $canvas
 *   An associative array containing the structure of the canvas.
 * @return
 *   The themed output as rendered by drupal_render().
 */
function drawing_render_canvas($canvas_id, &$canvas) {
  // Don't override #theme if someone already set it.
  if (!isset($canvas['#theme'])) {
    init_theme();
    $registry = theme_get_registry();
    if (isset($registry[$canvas_id])) {
      $canvas['#theme'] = $canvas_id;
    }
  }
  $output = drupal_render($canvas);
  return $output;
}

/**
 * Prepares a structured canvas array by adding required elements,
 * and executing any hook_drawing_canvas_alter functions.
 *
 * @param $canvas_id
 *   A unique string identifying the canvas for hook_drawing_canvas_alter
 *   functions.
 * @param $canvas
 *   An associative array containing the structure of the canvas.
 */
function drawing_prepare_canvas($canvas_id, &$canvas) {
  if ($canvas['#type'] == 'drawing_canvas') {
    $canvas += _element_info('drawing_canvas');
  }
  elseif ($canvas['#type'] == 'drawing_file') {
    $canvas += _element_info('drawing_file');
  }
  foreach (module_implements('drawing_canvas_alter') as $module) {
    $function = $module .'_drawing_canvas_alter';
    $function($form_id, $form);
  }
  return $canvas;
}

/**
 * Extract the defined method, defaults to 'drawing_svg'.
 */
function drawing_get_method() {
  static $drawing_method;
  if (empty($drawing_method)) {
    $drawing_method = variable_get('drawing_method', 'drawing_svg');
  }
  return $drawing_method;
}

/**
 * Get a list of drawing methods from other modules.
 */
function drawing_get_all_methods() {
  static $drawing_methods;
  if (empty($drawing_methods)) {
    $drawing_methods = module_invoke_all('drawing_method');
    if (empty($drawing_methods)) {
      drupal_set_message(t('Drawing API could not find any enabled <em>toolkit</em> modules.'), 'error');
    }
  }
  return $drawing_methods;
}

/**
 * Implementation of hook_elements().
 */
function drawing_elements() {
  $method = variable_get('drawing_method', 'drawing_svg');
  $type['drawing_canvas'] = array(
    '#width' => '200px',
    '#height' => '200px',
  );
  $type['drawing_group'] = array(
    '#width' => '200px',
    '#height' => '200px',
  );
  $type['drawing_file'] = array(
    '#height' => '200px',
    '#width' => '200px',
  );
  return $type;
}

/**
 * Theme function.
 */
function theme_drawing($element) {
  $method = drawing_get_method();
  return theme($method .'_'. $element['#type'], $element);
}

/*
  To do:
  
  chx says we should use '#process to tell the element that it's parent is svg.
  quote: "add a #process to the element and propagate down the value.
  for now I'm just applying svg

  Another idea; use preprocess functions in hook_theme to deal with the 'method'?

*/